<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$search_words = array(
    'php',
    'html',
    'Интернет',
    'web'
);


$search_strings = array(
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
    'PHP - это распространенный язык программирования  с открытым исходным кодом для Интернет.',
    'сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML',
    'PHP  непосредственно в Web'
);


function showWords2( $search_words, $search_strings){

    $numb = 1;
        foreach ($search_strings as $string) {
            echo '<b>В строке ' . $numb++ . ' найдены слова:</b> ';

            foreach ($search_words as $word) {

                preg_match_all('/'.$word .'/i', $string, $res);

                foreach ($res as $val) {

                    foreach ($val as $v) {
                        echo ' ' . $v . ' ';
                    }
                }
            }
            echo '<br>';
        }
    }


function showWords3( $search_words, $search_strings){

    $numb = 1;
    foreach ($search_strings as $string) {
        echo '<b>В строке ' . $numb++ . ' найдены слова:</b> ';

        foreach ($search_words as $word) {

            preg_match('/'.$word .'/i', $string, $res2);

            foreach ($res2 as $val) {
                echo ' ' . $val . ' ';
            }
        }
        echo '<br>';
    }
}
?>



<!DOCTYPE html>
<html lang="ru">
    <?php include_once 'include/head.php'?>
<body>



<div class="container">


    <div class="col-md-6">
        <h4>preg_match_all</h4>
    <?php echo showWords3($search_words, $search_strings); ?>
    </div>



    <div class="col-md-6">
        <h4>preg_match</h4>
    <?php echo showWords2($search_words, $search_strings); ?>
    </div>



</div>
</body>
</html>